(function ($, Drupal, drupalSettings) {
  Drupal.behaviors.linkObfuscator = {
    attach: function (context, settings) {
      $.each($('[data-link]'), function (index, value) {
        var target = $(this).attr("target") ? '_blank' : '_self';
        $(this).css("cursor", "pointer").click(function () {
          window.open(atob($(value).data('link')), target);
        })
      });
    }
  };
})(jQuery, Drupal, drupalSettings);
