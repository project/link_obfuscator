<?php

namespace Drupal\link_obfuscator\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\link\Plugin\Field\FieldFormatter\LinkFormatter;

/**
 * Plugin implementation of the 'link_obfuscator_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "link_obfuscator",
 *   label = @Translation("Link obfuscator"),
 *   field_types = {
 *     "link"
 *   }
 * )
 */
class LinkObfuscatorFormatter extends LinkFormatter {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'html_classes' => ''
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $elements = parent::settingsForm($form, $form_state);

    $elements['html_classes'] = [
      '#type' => 'textfield',
      '#title' => t('Additional css classes'),
      '#default_value' => $this->getSetting('html_classes'),
      '#description' => t('Add extra classes separated by space.'),
    ];

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    // Implement settings summary.

    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $parent_elements = parent::viewElements($items, $langcode);

    $new_elements = [];
    foreach ($parent_elements as $key => $element) {
      $element = [
        '#type' => 'html_tag',
        '#tag' => 'span',
        '#value' => $element['#title'],
        '#attributes' => [
          'data-link' => base64_encode($element['#url']->toString()),
          'class' => ['obflink'],
        ],
      ];
      if($this->settings['html_classes']){
        $element['#attributes']['class'][] = $this->settings['html_classes'];
      }
      if($this->settings['target']){
        $element['#attributes']['target'][] = True;
      }
      $new_elements[] = $element;
    }

    $new_elements['#attached']['library'][] = 'link_obfuscator/obfuscator';

    return $new_elements;
  }

  /**
   * Generate the output appropriate for one field item.
   *
   * @param \Drupal\Core\Field\FieldItemInterface $item
   *   One field item.
   *
   * @return string
   *   The textual output generated.
   */
  protected function viewValue(FieldItemInterface $item) {
    // The text value has no text format assigned to it, so the user input
    // should equal the output, including newlines.
    return nl2br(Html::escape($item->value));
  }

}
