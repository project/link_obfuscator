# Dynamic breadcrumb documentation

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Search engines bots (Google, Yahoo, Bing, Qwant, Yandex and many others…)
use a Page ranker system in order to classify pages in the search result,
so each page on the website has a specific rank based on how many links it includes.
This module gives the possibility to hide a specific link or links in a
page (keeping the same behavior of a normal clickable link) in order to control
 more the ranking of a page.

For more informations, see the full description on
https://www.drupal.org/project/link_obfuscator

REQUIREMENTS
------------

This module doesn't require any other module.

INSTALLATION
------------

Install the Link Obfuscator module as you would normally install a
contributed Drupal module.
Visit https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

This module doesn't have any configuration page, once installed and enabled, the module provides
a new Link field formatter called "Link obfuscator" that can be enabled for each specific link field,
once the formatter is activated, a new checkbox will appear in the content form that allows obfuscate each link.

MAINTAINERS
-----------

 * **Akram ZAIRIG** - Creator and Maintainer -
 https://www.drupal.org/u/akram-zairig
